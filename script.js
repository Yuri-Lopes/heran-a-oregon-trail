//Viajante
function Traveler(name, food, isHealty) {
    this.name = name;
    this.food = 1;
    this.isHealty = true;
}

Traveler.prototype.hunt = function () {
    this.food = this.food + 2;
};

Traveler.prototype.eat = function () {
    if (this.food <= 0) {
        this.isHealty = false;
    } else {
        this.food = this.food - 1;
    }
};
///////////////////////////////////
//Doctor

function Doctor(name, food, isHealty) {
    Traveler.call(this, name, food, isHealty);
}
Doctor.prototype = Object.create(Traveler.prototype);
Doctor.prototype.constructor = Doctor;

Doctor.prototype.heal = function (traveler) {
    traveler.isHealty = true;
}

//Hunter(caçador)

function Hunter(name, food, isHealty) {
    Traveler.call(this, name, food, isHealty);
    this.food = 2;
}

Hunter.prototype = Object.create(Traveler.prototype);
Hunter.prototype.constructor = Hunter;

Hunter.prototype.hunt = function () {
    this.food += 5;
}
Hunter.prototype.eat = function () {
    if (this.food > 1) this.food -= 2;
    else {
        this.food = 0;
        this.isHealty = false;
    }
};

Hunter.prototype.givefood = function (traveler, numOfFoodUnits) {
    if (this.food >= numOfFoodUnits) {
        this.food -= numOfFoodUnits;
        traveler.food = + numOfFoodUnits;
    }
}

// Vagão (Carroça)
function Wagon(capacity) {
    this.capacity = capacity;
    this.passengers = [];
}

Wagon.prototype.getAvailableSeatCount = function () {
    return this.capacity - this.passengers.length;
};

Wagon.prototype.join = function (traveler) {
    if (this.getAvailableSeatCount() > 0) {
        this.passengers.push(traveler);
    }
};

Wagon.prototype.shouldQuarantine = function () {
    for (let i = 0; i < this.passengers.length; i++) {
        if (this.passengers[i].isHealty === false)
            return true;
    }
    return false;
};

Wagon.prototype.totalFood = function () {
    let total = 0;
    this.passengers.forEach((traveler) => {
        total = total + traveler.food;
    });

    return total;
};



// Cria uma carroça que comporta 4 pessoas
let wagon = new Wagon(4);
// Cria cinco viajantes
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let drsmith = new Doctor('Dr. Smith');
let sarahunter = new Hunter('Sara');
let maude = new Traveler('Maude');

console.log(`#1: There should be 4 available seats. Actual: ${wagon.getAvailableSeatCount()}`);

wagon.join(henrietta);
console.log(`#2: There should be 3 available seats. Actual: ${wagon.getAvailableSeatCount()}`);

wagon.join(juan);
wagon.join(drsmith);
wagon.join(sarahunter);

wagon.join(maude); // Não tem espaço para ela!
console.log(`#3: There should be 0 available seats. Actual: ${wagon.getAvailableSeatCount()}`);

console.log(`#4: There should be 5 total food. Actual: ${wagon.totalFood()}`);

sarahunter.hunt(); // pega mais 5 comidas
drsmith.hunt();

console.log(`#5: There should be 12 total food. Actual: ${wagon.totalFood()}`);

henrietta.eat();
sarahunter.eat();
drsmith.eat();
juan.eat();
juan.eat(); // juan agora está doente (sick)

console.log(`#6: Quarantine should be true. Actual: ${wagon.shouldQuarantine()}`);
console.log(`#7: There should be 7 total food. Actual: ${wagon.totalFood()}`);

drsmith.heal(juan);
console.log(`#8: Quarantine should be false. Actual: ${wagon.shouldQuarantine()}`);

sarahunter.giveFood(juan, 4);
sarahunter.eat(); // Ela só tem um, então ela come e fica doente

console.log(`#9: Quarantine should be true. Actual: ${wagon.shouldQuarantine()}`);
console.log(`#10: There should be 6 total food. Actual: ${wagon.totalFood()}`);